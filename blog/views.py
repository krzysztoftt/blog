from django.shortcuts import render , get_object_or_404
from .models import Post, Comment
from django.views.generic import ListView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail
from .forms import EmailPostForm , CommentForm , SearchForm
from taggit.models import Tag
from django.db.models import Count
from django.contrib.postgres.search import SearchVector

class PostListView(ListView):
	
	context_object_name = 'posts'
	paginate_by = 3
	template_name = 'blog/post/list.html'
	allow_empty = False
	tag = None
	
	def get_queryset(self):
		try:
			tag_slug = self.kwargs['tag_slug']
			if ( tag_slug ):
				#tag_slug = self.kwargs['tag_slug']
				tag = get_object_or_404(Tag,slug=tag_slug)
				posts = Post.published.all()
				return posts.filter(tags__in=[tag])
		except KeyError:
				
			return Post.published.all()
	
	def get_context_data(self, **kwargs):
		context = super(PostListView, self).get_context_data(**kwargs)
		try:
			tag_slug = self.kwargs['tag_slug']
			context['tag'] = Tag.objects.get(slug=tag_slug)
			return context
		except KeyError:
			return context



def post_detail(request, year, month, day, post):
	post = get_object_or_404(Post, status='published',publish__year=year, publish__month=month, publish__day=day, slug=post)
	
	comments = post.comments.filter(active=True)

	if request.method == 'POST':
		comment_form = CommentForm(data=request.POST)
		if comment_form.is_valid():
			new_comment = comment_form.save(commit=False)
			new_comment.post = post
			new_comment.save()
	else:
		comment_form = CommentForm()
		new_comment = False
	post_tags_ids = post.tags.values_list('id',flat=True)
	similar_posts = Post.published.filter(tags__in=post_tags_ids).exclude(id=post.id)
	similar_posts = similar_posts.annotate(same_tags=Count('tags')).order_by('-same_tags','-publish')[:4]
	return render(request,'blog/post/detail.html', {'post': post,'comments': comments,'comment_form': comment_form, 'new_comment': new_comment, 'similar_posts': similar_posts})

def post_share(request, post_id):
	post = get_object_or_404(Post, id=post_id, status='published')
	sent = False

	if request.method == 'POST':
		form = EmailPostForm(request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			post_url = request.build_absolute_uri(post.get_absolute_url())
			subject = '{} ({}) zachęcam do przeczytania "{}"'.format(cd['name'], cd['email'], post.title)
			message = 'Przeczytaj post "{}" na stronie {} \n \n Komentarz dodany przez {}: {}'.format(post.title, post_url, cd['name'],cd['comments'])
			send_mail(subject, message, 'admin@myblog.com', [cd['to']],fail_silently=True)
			sent = True
	else:
		form = EmailPostForm()
	return render(request, 'blog/post/share.html',{'post': post,'form': form, 'sent':sent})

def post_search(request):
	form = SearchForm
	query = None
	results = []
	if 'query' in request.GET:
		form = SearchForm(request.GET)
		if form.is_valid():
			query = form.cleaned_data['query']
			results = Post.objects.annotate(search=SearchVector('title', 'body'),).filter(search=query)
	return render(request,'blog/post/search.html', {'form': form, 'query': query, 'results':results})