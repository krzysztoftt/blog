import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysite.settings')
import django ,random
django.setup()
from faker import Faker
from blog.models import *
from django.utils import timezone
from django.contrib.auth.models import User
from django.utils.text import slugify



fake = Faker('EN')

tags = ['food', 'sport' ,'informatics' ,'mobile' , 'photographs' , 'science']

def create_user(N=10):
	
	for i in range(N):
		User.objects.create(username=fake.user_name()+str(random.randint(0,99)), email=fake.email(), password='testpass')

def create_post(N=10):
	
	for _ in range(N):
		title = fake.sentence()
		author = User.objects.order_by('?').first()# random user 
		created = fake.date_time_between(start_date='-5y',end_date='-2d')
		body = fake.paragraph() + " " + fake.paragraph() + " " + fake.paragraph()+ " " + fake.paragraph() + " " + fake.paragraph()+ " " + fake.paragraph() + " " + fake.paragraph()+ " " + fake.paragraph() + " " + fake.paragraph()
		publish = fake.date_time_between(start_date='-5d',end_date='-2d')
		Post.objects.create(title=title, author=author, slug=slugify(title), created=created, body=body, status='published',publish=publish)


def create_comment_all_post(N=10):
	post = Post.objects.all()
	for p in post:
		for _ in range(N):
			name = fake.user_name()
			email = fake.email()
			body = fake.sentence()
			Comment.objects.create(post=p,name=name, email=email, body=body)

def add_tags():
	post = Post.objects.all()
	for p in post:
		N = random.randint(1,4)
		for _ in range(N):
			tag = random.choices(tags)
			p.tags.add("".join(tag))



if __name__ == '__main__':
	print('Genering data ...')
	print('Create user ....')
	create_user(100)
	print('Create posts ...')
	create_post(40)
	print("Create comment ...")
	create_comment_all_post()
	print("Add tags ...")
	add_tags()
	print('Genering data finish.')
