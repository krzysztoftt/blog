import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysite.settings')
import django 
django.setup()

from blog.models import *
from django.contrib.auth.models import User

def erase_user():
	User.objects.all().exclude(username='admin').delete()

if __name__ == '__main__':
	print("Delete user..")
	erase_user()
	print("Complete")
